//
//  HighSchoolDetailModel.swift
//  20230314-AdinarayanaBoddapati-NYCSchools
//
//  Created by Aadi on 3/14/23.
//

import Foundation

struct HighSchoolDetailModel: Codable {
    let dbn: String
    let schoolName: String
    let numberOfTestTakers: String
    let criticalReadingMean: String
    let mathematicsMean: String
    let writingMean: String
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingMean = "sat_critical_reading_avg_score"
        case mathematicsMean = "sat_math_avg_score"
        case writingMean = "sat_writing_avg_score"
        
    }
}
